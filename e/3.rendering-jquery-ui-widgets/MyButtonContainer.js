import React, { Component } from 'react';
import { fromJS } from 'immutable';

import MyButton from './MyButton';

class MyButtonContainer extends Component {
  state = {
    data: fromJS({}),
  }

  get data() {
    return this.state.data;
  }

  set data(data) {
    this.setState({ data });
  }

  componentWillMount() {
    this.data = this.data
      .merge(this.props, {
        onClick: this.props.onClick.bind(this), // eslint-disable-line react/prop-types
      });
  }

  render() {
    return (
      <MyButton {...this.state.data.toJS()} />
    );
  }
}

MyButtonContainer.defaultProps = {
  onClick: () => {},
};

export default MyButtonContainer;
