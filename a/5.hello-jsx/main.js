import React from 'react';
import {render} from 'react-dom';

// Renders the JSX markup. Notice the XML syntax
// mixed with JavaScript? This is replaced by the
// transpiler before it reaches the browser.
render(
    (<p>Hello, <strong>JSX</strong></p>),
    document.getElementById('app')
);
