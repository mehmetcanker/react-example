import React from 'react';
import { render } from 'react-dom';


console.log('available tags',
  Object.keys(React.DOM).sort());


render((
  <div>
    <button />
    <code />
    <input />
    <label />
    <p />
    <pre />
    <select />
    <table />
    <ul />
  </div>
  ),
  document.getElementById('app')
);
