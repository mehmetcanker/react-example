import React from 'react';
import { render } from 'react-dom';

import MyComponent from './MyComponent';

const myComponent = render(
  (<MyComponent />),
  document.getElementById('app')
);

setTimeout(() => {
  myComponent.setState({ first: 'done!' });
}, 1000);

setTimeout(() => {
  myComponent.setState({ second: 'done!' });
}, 2000);

setTimeout(() => {
  myComponent.setState({ third: 'done!' });
}, 3000);
